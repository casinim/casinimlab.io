var wins = 0;
var loses = 0;

var winSpan = document.querySelector('#wins');
var loseSpan = document.querySelector('#loses');
var percentSpan = document.querySelector('#percent');

async function spin() {
  let response = await new Promise(resolve => {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'http://localhost:26111/slot', true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = function(e) {
      resolve(xhr.response);
    };
    xhr.onerror = function () {
      resolve(undefined);
      console.error("** An error occurred during the XMLHttpRequest");
    };
    xhr.send();
  });
  return JSON.parse(response);
}

async function test(runs) {
  var wins = {
    '1x 0': 0,
    '2x 0': 0,
    '3x 0': 0,
    '3x 1': 0,
    '3x 2': 0,
    '3x 3': 0,
    '3x 4': 0,
    '3x 5': 0,
    'no win': 0
  }

  for(i = 0; i < runs; i++) {
    var spinWin = await spin();

    wins[spinWin.win] += 1;

    // if(spinWin.win) {
    //   wins++;
    //   winSpan.innerHTML = wins;
    // }
    // else {
    //   loses++;
    //   loseSpan.innerHTML = loses;
    // } 
    percentSpan.innerHTML = parseInt(1 + i * 100 / runs);
  };

  console.table({
    wins
  });
}

test(10000);